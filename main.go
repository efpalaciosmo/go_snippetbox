package main

import (
    "log"
    "net/http"
    "fmt"
    "strconv"
)

// Define a home handler function wich writes a byte slice containing
// "Hello from snippetbox" as the response body

func home(w http.ResponseWriter, r *http.Request) {
    if r.URL.Path != "/"{
        http.NotFound(w, r)
        return
    }
    w.Write([]byte("Hello from Snippetbox"))
}
func showSnippet(w http.ResponseWriter, r *http.Request) {
    id, err := strconv.Atoi(r.URL.Query().Get("id"))
    if err != nil || id < 1{
        http.NotFound(w, r)
        return
    }
    fmt.Fprintf(w, "Display a specific snippet with ID %d", id)
}
func createSnippet(w http.ResponseWriter, r *http.Request) {
    if r.Method != http.MethodPost {
        w.Header().Set("Allow", http.MethodPost)
        http.Error(w, "Method Not Allowed", 405)
        return
    }
    w.Write([]byte("Create a new snippet..."))
}

func main(){
    // Use the http.NewServeMux() function to initialize a new servemux, then
    // register the home as the handler for the "/" URL pattern
    mux := http.NewServeMux()
    mux.HandleFunc("/", home)
    mux.HandleFunc("/snippet", showSnippet)
    mux.HandleFunc("/snippet/create", createSnippet)

    log.Println("Starting server on :4000")
    err := http.ListenAndServe(":4000", mux)
    log.Fatal(err)
}
